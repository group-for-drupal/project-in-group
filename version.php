<?php

/**
 * @package    local_ned9hub
 * @subpackage NED
 * @copyright  2023 NED {@link http://ned.ca}
 * @author     NED {@link http://ned.ca}
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version  = 2023033000;    // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires = 2017051500.00; // This is Moodle 3.3 (Build: 20170515).
$plugin->maturity = MATURITY_ALPHA;
$plugin->component = 'local_ned9hub';
$plugin->release = '3.3.0.1.13';